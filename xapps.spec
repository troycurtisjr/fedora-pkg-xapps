Name:           xapps
Version:        1.0.4
Release:        4%{?dist}
Summary:        Common files for XApp desktop apps

License:        LGPLv2+
URL:            https://github.com/linuxmint
Source0:        %url/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source1:        %url/flags/archive/1.0.1.tar.gz#/flags-1.0.1.tar.gz

BuildRequires:  gnome-common
BuildRequires:  gtk-doc
BuildRequires:  glib2-devel
BuildRequires:  intltool
BuildRequires:  gobject-introspection-devel
BuildRequires:  pygobject3-devel
BuildRequires:  python2-rpm-macros
BuildRequires:  python3-rpm-macros
BuildRequires:  libX11-devel
BuildRequires:  gtk3-devel
BuildRequires:  libgnomekbd-devel
%if 0%{?fedora} || 0%{?rhel} >= 8
Requires:       python3-gobject-base
%else
Requires:       pygobject3
%endif
Requires:       inxi
Requires:       xdg-utils
Requires:       fpaste
Provides:       python3-xapps-overrides%{?_isa} = %{version}-%{release}

%description
This package includes files that are shared between several XApp
apps (i18n files and configuration schemas).

%package     -n python2-xapps-overrides
Summary:        Python2 files for %{name}
Requires:       python2-gobject-base
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description -n python2-xapps-overrides
Python2 files for XApp apps.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
Development libraries and header files for
developing XApp apps.

%prep
%autosetup -p1
tar -xf %{SOURCE1} -C files/usr/share --strip 3
rm files/usr/share/format

%if (0%{?rhel} && 0%{?rhel} >= 7)
for f in $(%{__grep} -Rl '#!.*python3') ; do
  %{__sed} -e 's~#!.*python3~#!/usr/bin/python2~g' < ${f} > ${f}.new
  /bin/touch -r ${f}.new ${f}
  mode="$(%{_bindir}/stat -c '%a' ${f})"
  %{__mv} -f ${f}.new ${f}
  %{__chmod} -c ${mode} ${f}
done
%endif # (0%%{?rhel} && 0%%{?rhel} >= 7)

NOCONFIGURE=1 ./autogen.sh

%build
%global _configure ../configure
mkdir -p build-py2 build-py3
cp -Rp files/ build-py2/
pushd build-py2 
export PYTHON=%{__python2}
%configure
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool
%make_build V=1
popd

pushd build-py3
export PYTHON=%{__python3}
%configure
popd


%install
pushd build-py2
%make_install
popd

pushd build-py3
cd pygobject/
%make_install
popd

find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%post
/sbin/ldconfig
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files
%license COPYING
%doc README.md
%{_bindir}/pastebin
%{_bindir}/upload-system-info
%{_bindir}/xfce4-set-wallpaper
%{_libdir}/libxapp.so.*
%{_libdir}/girepository-1.0/XApp-1.0.typelib
%{_datadir}/iso-flag-png/
%{_datadir}/glib-2.0/schemas/org.x.apps.*.xml
%{_datadir}/icons/hicolor/scalable/actions/*.svg
%{python3_sitearch}/gi/overrides/XApp.py
%{python3_sitearch}/gi/overrides/__pycache__/XApp.cpython-36*.pyc

%files -n python2-xapps-overrides
%{python2_sitearch}/gi/overrides/XApp.py*

%files devel
%{_includedir}/*
%{_libdir}/libxapp.so
%{_libdir}/pkgconfig/xapp.pc
%{_datadir}/gir-1.0/XApp-1.0.gir
%{_datadir}/glade/catalogs/xapp-glade-catalog.xml
%{_datadir}/vala/vapi/xapp.vapi


%changelog
* Mon Nov 13 2017 Troy Curtis, Jr <troycurtisjr@gmail.com> - 1.0.4-4
- Have python2-xapps-overrides require xapps instead of the other way around.
- Use python macros

* Sat Nov 11 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.0.4-3
- Add requires python2-gobject-base

* Thu Oct 26 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.0.4-2
- build python XApp overrides

* Tue Oct 24 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.0.4-1
- update to 1.0.4 release

* Thu Aug 31 2017 Björn Esser <besser82@fedoraproject.org> - 1.0.3-6
- Preserve mode of files when changing hashbang

* Tue Aug 29 2017 Björn Esser <besser82@fedoraproject.org> - 1.0.3-5
- Use Python2 on epel

* Mon Aug 28 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.0.3-4
- Fix requires for epel

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed May 03 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.0.3-1
- update to 1.0.3 release
- add build requires gtk-doc

* Thu Feb 23 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.0.2-5
- Add python3-gobject-base instead of python-gobject-base

* Thu Feb 23 2017 Leigh Scott <leigh123linux@googlemail.com> - 1.0.2-4
- Add some upstream fixes

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 1.0.2-2
- Rebuild for Python 3.6

* Mon Nov 07 2016 Leigh Scott <leigh123linux@googlemail.com> - 1.0.2-1
- update to 1.0.2 release

* Sat Nov 05 2016 Leigh Scott <leigh123linux@googlemail.com> - 1.0.0-0.3.gita8d5277
- update to latest git

* Tue Oct 11 2016 Leigh Scott <leigh123linux@googlemail.com> - 1.0.0-0.2.git0f28d18
- fix review issues

* Sat Oct 08 2016 Leigh Scott <leigh123linux@googlemail.com> - 1.0.0-0.1.git7e7567a
- first build
